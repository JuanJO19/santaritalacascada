<?php

namespace BackendBundle\Controller;

use BackendBundle\Form\ContentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use BackendBundle\Form\VideoType;
use BackendBundle\Entity\Image;
use BackendBundle\Entity\Video;
use BackendBundle\Entity\Content;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller {

    /**
     * metodo para agregar multiples imagenes en la seccion uno
     * @Route("/section1/new", name="section_one_add_backend")
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $images = $request->files;
            foreach ($images as $image) {
                $fileName = md5(uniqid()) . '.' . $image->getClientOriginalExtension();
                $sizeFile = $image->getClientSize();
                $image->move(
                        $this->getParameter('images_directory'), $fileName
                );
                $entity = new Image();
                $entity->setCategoria('section1');
                $entity->setImage($fileName);

                $em->persist($entity);
            }
            $em->flush();
            return $this->redirect($this->generateUrl('detail_one_backend'));
        }
        return $this->render('BackendBundle:Default:index.html.twig', array());
    }

    /**
     * metodo para agregar multiples imagenes en la seccion uno
     * @Route("/section2/new", name="section_two_add_backend")
     */
    public function indexSecondAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $images = $request->files;
            foreach ($images as $image) {
                $fileName = md5(uniqid()) . '.' . $image->getClientOriginalExtension();
                $sizeFile = $image->getClientSize();
                $image->move(
                        $this->getParameter('images_directory'), $fileName
                );
                $entity = new Image();
                $entity->setCategoria('section2');
                $entity->setImage($fileName);

                $em->persist($entity);
            }
            $em->flush();
            return $this->redirect($this->generateUrl('detail_two_backend'));
        }
        return $this->render('BackendBundle:Default:indexTwo.html.twig', array());
    }
    
    /**
     * metodo para agregar multiples imagenes en la seccion uno
     * @Route("/section3/new", name="section_three_add_backend")
     */
    public function indexThreedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $images = $request->files;
            foreach ($images as $image) {
                $fileName = md5(uniqid()) . '.' . $image->getClientOriginalExtension();
                $sizeFile = $image->getClientSize();
                $image->move(
                        $this->getParameter('images_directory'), $fileName
                );
                $entity = new Image();
                $entity->setCategoria('section3');
                $entity->setImage($fileName);

                $em->persist($entity);
            }
            $em->flush();
            return $this->redirect($this->generateUrl('detail_three_backend'));
        }
        return $this->render('BackendBundle:Default:indexThree.html.twig', array());
    }

    /**
     * @Route("/videoSlider/home", name="video_backend")
     */
    public function videoAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $existvideo = $em->getRepository('BackendBundle:Video')->findOneBy(array());
        $video = new Video();
        if (sizeof($existvideo) == 1) {
            $video = $existvideo;
        }
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($video);
            $em->flush();
            return $this->redirect($this->generateUrl('video_backend'));
        }
        return $this->render('BackendBundle:Default:video.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * metodo para mostrar el detalle las imagenes de la seccion 1 del frontend
     * @Route("/section1/detailImages", name="detail_one_backend")
     */
    public function detailOneAction() {

        $em = $this->getDoctrine()->getManager();
        $registers = $em->getRepository('BackendBundle:Image')->findBy(array('categoria' => 'section1'));
        return $this->render('BackendBundle:Default:detailImage1.html.twig', array(
                    'registers' => $registers
        ));
    }

    /**
     * metodo para mostrar el detalle las imagenes de la seccion 2 del frontend
     * @Route("/section2/detailImages", name="detail_two_backend")
     */
    public function detailTwoAction() {

        $em = $this->getDoctrine()->getManager();
        $registers = $em->getRepository('BackendBundle:Image')->findBy(array('categoria' => 'section2'));
        return $this->render('BackendBundle:Default:detailImage2.html.twig', array(
                    'registers' => $registers
        ));
    }
    
    /**
     * metodo para mostrar el detalle las imagenes de la seccion 2 del frontend
     * @Route("/section3/detailImages", name="detail_three_backend")
     */
    public function detailThreeAction() {

        $em = $this->getDoctrine()->getManager();
        $registers = $em->getRepository('BackendBundle:Image')->findBy(array('categoria' => 'section3'));
        return $this->render('BackendBundle:Default:detailImage3.html.twig', array(
                    'registers' => $registers
        ));
    }

    /**
     * metodo para editar la imagen
     * @Route("/image/edit/{id}/{type}", name="edit_image_backend")
     */
    public function editImageAction($id, $type, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('BackendBundle:Image')->find($id);
        //imagen actual que será eliminada despues de ser editada
        $fileAct = $image->getImage();
        $form = $this->createForm(\BackendBundle\Form\ImageEditType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            unlink('uploads/images/' . $fileAct);

            $file = $image->getImage();
            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                    $this->getParameter('images_directory'), $fileName
            );
            $image->setImage($fileName);
            $em->persist($image);
            $em->flush();
            switch ($type) {
                case 'one':
                    return $this->redirect($this->generateUrl('detail_one_backend'));
                    break;
                case 'two':
                    return $this->redirect($this->generateUrl('detail_two_backend'));
                    break;
                case 'three':
                    return $this->redirect($this->generateUrl('detail_three_backend'));
                    break;
            }
        }
        return $this->render('BackendBundle:Default:editImage1.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * metodo que permite eliminar un registro de imagen
     * @Route("/image/delete", name="delete_image_backend")
     */
    public function deleteProductAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Image')->find($id);
        $fileAct = $entity->getImage();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find image entity.');
        }
        unlink('uploads/images/' . $fileAct);
        $em->remove($entity);
        $em->flush();
        $response = new JsonResponse(
                array(
            'result' => '__OK__',
            'msg' => 'Try again'
        ));

        return $response;
    }

    /**
     * @Route("/content/home/{section}", name="content_backend")
     */
    public function contentAction(Request $request, $section) {

        $em = $this->getDoctrine()->getManager();
        $existContent = $em->getRepository('BackendBundle:Content')->findOneBy(array('section' => $section));
        $content = new Content();
        if (sizeof($existContent) == 1) {
            $content = $existContent;
        }
        $form = $this->createForm(ContentType::class, $content);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $content->setSection($section);
            $content->setContent($request->request->get('editor1'));
            $em->persist($content);
            $em->flush();
            return $this->redirect($this->generateUrl('content_backend', array('section'=>$section)));
        }
        return $this->render('BackendBundle:Default:content.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/inbox/home", name="content_inbox")
     */
    public function inboxAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Contact')->findAll();

        return $this->render('BackendBundle:Default:inbox.html.twig', array(
            'entity' => $entity
        ));
    }

    /**
     * @Route("/inbox/response/{idmail}", name="content_inbox_response")
     */
    public function inboxResponseAction(Request $request, $idmail) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Contact')->find($idmail);

        if ($request->isMethod('POST')) {
            $mensaje = $request->request->get('response');
            $mail = \Swift_Message::newInstance()
                ->setSubject('Contacto Santa Rita La Cascada')
                ->setFrom('santaritalacascada@gmail.com')
                ->setTo($entity->getEmail())
                ->setBody(
                    $this->renderView(
                        'BackendBundle:Default:mail.html.twig',
                        array('entity' => $entity, 'mensaje' => $mensaje)
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($mail);
            //se setea el correo como leido o respondido
            $entity->setLeido(true);
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('content_inbox'));
        }

        return $this->render('BackendBundle:Default:responseMail.html.twig', array(
            'entity' => $entity,
            'idmail' => $idmail
        ));
    }

    /**
     * metodo que permite eliminar un registro de correo
     * @Route("/inbox/delete", name="delete_inbox_backend")
     */
    public function deleteInboxAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Contact')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find contact entity.');
        }
        $em->remove($entity);
        $em->flush();
        $response = new JsonResponse(
            array(
                'result' => '__OK__',
                'msg' => 'Try again'
            ));

        return $response;
    }

}
