<?php

namespace BackendBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use BackendBundle\Entity as Entity;

class DataLoad extends Fixture {

    public function load(ObjectManager $manager) {
        /*$users = array(
            array(
                'type' => Entity\User::USER_ADMINISTRATOR,
                'name' => 'Super Admin',
                'lastname' => '',
                'email' => 'admin@energizando.com',
                'address' => '',
                'password' => 'energizando321654987/*',
            ),
            array(
                'type' => Entity\User::USER_WEB_MANAGER,
                'name' => 'Web Manager',
                'lastname' => '',
                'email' => 'webmanager@energizando.com',
                'address' => '',
                'password' => 'uEvN8yM',
            ),
        );

        foreach ($users as $item) {
            $User = new Entity\User();
            $User->setType($item['type']);
            $User->setName($item['name']);
            $User->setLastName($item['lastname']);
            $User->setEmail($item['email']);
            $User->setSalt();

            $encoder = $this->container->get('security.encoder_factory')->getEncoder($User);
            $password = $encoder->encodePassword($item['password'], $User->getSalt());
            $User->setPassword($password);

            $manager->persist($User);
        }*/

        $content = array(
            array(
                'section' => 'section1',
                'content' => 'Contenido Administrable'
            ),
            array(
                'section' => 'section2',
                'content' => 'Contenido Administrable'
            ),
            array(
                'section' => 'section3',
                'content' => 'Contenido Administrable'
            ),
            array(
                'section' => 'section_home',
                'content' => 'Contenido Administrable'
            ),
            array(
                'section' => 'section_terms',
                'content' => 'Contenido Administrable'
            )
        );

        foreach ($content as $item) {
            $entity = new Entity\Content();
            $entity->setSection($item['section']);
            $entity->setContent($item['content']);

            $manager->persist($entity);
        }
        
        $manager->flush();
    }

}