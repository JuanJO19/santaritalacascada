<?php

namespace BackendBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="contact")
 * @ORM\Entity
 */
class Contact {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * nombre del contacto
     * @ORM\Column(type="string", name="contact_name", length=100)
     */
    private $nombre;

    /**
     * email del contacto
     * @Assert\Email(
     *     message = "El correo '{{ value }}' no es valido.",
     *     checkMX = true
     * )
     * @ORM\Column(type="string", name="contact_email")
     */
    private $email;

    /**
     * asunto del contacto
     * @ORM\Column(type="string", name="contact_asunto", length=100)
     */
    private $asunto;

    /**
     * mensaje 
     * @ORM\Column(type="text", name="contact_mensaje")
     */
    private $mensaje;

    /**
     * variable para saber si el correo ha sido leido 
     * @ORM\Column(type="boolean", name="contact_leido")
     */
    private $leido;

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function getLeido() {
        return $this->leido;
    }

    function setLeido($leido) {
        $this->leido = $leido;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getEmail() {
        return $this->email;
    }

    function getAsunto() {
        return $this->asunto;
    }

    function getMensaje() {
        return $this->mensaje;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setAsunto($asunto) {
        $this->asunto = $asunto;
    }

    function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }

}
