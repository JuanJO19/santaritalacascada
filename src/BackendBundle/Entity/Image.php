<?php

namespace BackendBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="image")
 * @ORM\Entity
 */
class Image {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * categoria
     * @ORM\Column(type="string", length=100)
     */
    private $categoria;

    /**
     * @ORM\Column(type="string", name="img_path")
     *
     * @Assert\NotBlank(message="debe subir una imagen.")
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/pjpeg" },  mimeTypesMessage = "Extensión de archivo inválida (.PNG - .JPEG - .PJPEG)")
     */
    private $image;

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function getCategoria() {
        return $this->categoria;
    }

    function getImage() {
        return $this->image;
    }

    function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    function setImage($image) {
        $this->image = $image;
    }

}
