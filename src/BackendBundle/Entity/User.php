<?php

namespace BackendBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user",uniqueConstraints={@ORM\UniqueConstraint(name="uniq_user_email", columns={"user_email"})})
 * @ORM\Entity
 */
class User implements UserInterface {

    //Tipos de usuarios
    const USER_ADMINISTRATOR = 1;
    const USER_WEB_MANAGER = 2;
    const USER_WEB_USER = 3;

    /**
     * @ORM\Id
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="user_name", type="string", length=100) 
     */
    protected $name;

    /**
     * @ORM\Column(name="user_lastName", type="string", length=100, nullable=true) 
     */
    protected $lastName;

    /**
     * @ORM\Column(name="user_email", type="string", length=255) 
     */
    protected $email;

    /**
     * @ORM\Column(name="user_password", type="string", length=255,nullable=true) 
     */
    protected $password;

    /**
     * @ORM\Column(name="user_role", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    protected $role;

    /**
     * @ORM\column(name="user_salt", type="text",nullable=true) 
     */
    protected $salt;

    public function getId() {
        return $this->id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getName() {
        return $this->name;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function eraseCredentials() {
        
    }

    public function getSalt() {
        return $this->salt;
    }

    public function getUsername() {
        return $this->getEmail();
    }

    public function setSalt() {
        mt_srand();
        $this->salt = md5(time() . mt_rand());
    }

    public function getRoles() {
        if ($this->type == self::USER_ADMINISTRATOR) {
            return array('ROLE_ADMIN');
        } elseif ($this->type == self::USER_WEB_MANAGER) {
            return array('ROLE_WEB_MANAGER');
        } elseif ($this->type == self::USER_WEB_USER) {
            return array('ROLE_WEB_USER');
        } else {
            return array('ROLE_INACTIVE');
        }
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->email,
            $this->salt
        ));
    }

    public function unserialize($serialized) {
        list(
                $this->id,
                $this->email,
                $this->salt
                ) = unserialize($serialized);
    }

    function getRole() {
        return $this->role;
    }

    function setRole($role) {
        $this->role = $role;
    }

}
