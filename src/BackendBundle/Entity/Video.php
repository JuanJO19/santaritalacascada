<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="video")
 * @ORM\Entity
 */
class Video {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * link del video, url
     * @ORM\Column(type="string", length=200)
     */
    private $linkVideo;

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function getLinkVideo() {
        return $this->linkVideo;
    }

    function setLinkVideo($linkVideo) {
        $this->linkVideo = $linkVideo;
    }
}
