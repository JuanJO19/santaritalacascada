<?php

namespace BackendBundle\Form;

use BackendBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('nombre', TextType::class, array('required' => true))
                ->add('email', EmailType::class, array('required' => true))
                ->add('asunto', ChoiceType::class, array(
                    'choices' => array(
                        'Reserva' => 'reserva',
                        'Información' => 'información',
                        'Sugerencia' => 'sugerencia',
                    ),
                    'placeholder' => 'Asunto',
                ))
                ->add('mensaje', TextareaType::class, array('required' => true));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Contact::class
        ));
    }

}
