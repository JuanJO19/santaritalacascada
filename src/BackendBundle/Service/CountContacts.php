<?php

namespace BackendBundle\Service;

use Doctrine\ORM\EntityManagerInterface;

class CountContacts {

    protected $em;
    
    public function __construct(EntityManagerInterface $entityManager) {
        $this->em = $entityManager;
    }

    public function getNumberContacts() {
         $em = $this->em;
         $mails = count($em->getRepository('BackendBundle:Contact')->findBy(array('leido'=>false)));
         
         return $mails;
    }

}
