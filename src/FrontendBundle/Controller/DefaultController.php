<?php

namespace FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BackendBundle\Entity\Contact;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="frontend_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('BackendBundle:Video')->findOneBy(array());
        $sec_images3 = $em->getRepository('BackendBundle:Image')->findBy(array('categoria'=>'section3'));
        $sec_content3 = $em->getRepository('BackendBundle:Content')->findOneBy(array('section'=>'section3'));
        $sec_content2 = $em->getRepository('BackendBundle:Content')->findOneBy(array('section'=>'section2'));
        $sec_images2 = $em->getRepository('BackendBundle:Image')->findBy(array('categoria'=>'section2'));
        $sec_content1 = $em->getRepository('BackendBundle:Content')->findOneBy(array('section'=>'section1'));
        $sec_images1 = $em->getRepository('BackendBundle:Image')->findBy(array('categoria'=>'section1'));
        $sec_content_about = $em->getRepository('BackendBundle:Content')->findOneBy(array('section'=>'section_home'));
        $sec_content_terms = $em->getRepository('BackendBundle:Content')->findOneBy(array('section'=>'section_terms'));

        $entity = new Contact();
        $form = $this->createForm(\BackendBundle\Form\ContactType::class, $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setLeido(false);
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('frontend_index', array('flag'=>true)));
        }
        
        return $this->render('FrontendBundle:Default:index.html.twig', array(
            'video'=>$video,
            'form' => $form->createView(),
            'ima_section3' => $sec_images3,
            'ima_section2' => $sec_images2,
            'ima_section1' => $sec_images1,
            'cont_section3' => $sec_content3,
            'cont_section2' => $sec_content2,
            'cont_section1' => $sec_content1,
            'cont_section_home' => $sec_content_about,
            'cont_section_terms' => $sec_content_terms,
        ));
    }
}
